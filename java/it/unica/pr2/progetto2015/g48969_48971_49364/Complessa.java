package it.unica.pr2.progetto2015.g48969_48971_49364;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.*;

/** *
 *	@author 48969 Azzaro Edoardo - 48971 Palla Mattia - 49364 Piras Cesare
 *  <p>Calcola la matrice inversa della matrice 
 *	passata in ingresso al metodo execute tramite var args</p>
 */

public class Complessa implements SheetFunction {
	


	/**
	 * Costruttore vuoto.
	 */
	public void Complessa(){}
	
	/**
	 * Lancia il calcolo della matrice inversa della matrice passata tramite var args
	 * La matrice DEVE ESSERE QUADRATA  
	 * @param args Object ... args castati a Double. Elementi della matrice di cui calcolare l'inversa
	 * @return x   double[][] x   Matrice invertita 
	 */

	public Object execute(Object... args) {
		double[][] x = (double[][])args;
		return inverti(x);
	}
	/**
	 * Esegue il calcolo della matrice inversa con gli argomenti passati da execute
	 * @param  a matrice di double  Matrice da invertire
	 * @return x Matrice double[][] x Matrice invertita 
	 */
	// Metodo che inverte la matrice
	public static double[][] inverti(double a[][]) 
    {
        int n = a.length;
        double x[][] = new double[n][n];
        double b[][] = new double[n][n];
        int indice[] = new int[n];
        for (int i=0; i<n; ++i) 
            b[i][i] = 1;
 
 		// Trasformo la matrice in una matrice triangolare superiore
        gaussian(a, indice);
 
 		// Aggiorno la matrice b[i][j]
        for (int i=0; i<n-1; ++i)
            for (int j=i+1; j<n; ++j)
                for (int k=0; k<n; ++k)
                    b[indice[j]][k] -= a[indice[j]][i]*b[indice[i]][k];
 
 		// Eseguo la sostituzione all'indietro
        for (int i=0; i<n; ++i) 
        {
            x[n-1][i] = b[indice[n-1]][i]/a[indice[n-1]][n-1];
            for (int j=n-2; j>=0; --j) 
            {
                x[j][i] = b[indice[j]][i];
                for (int k=j+1; k<n; ++k) 
                {
                    x[j][i] -= a[indice[j]][k]*x[k][i];
                }
                x[j][i] /= a[indice[j]][j];
            }
        }
        return x;
    }
	
	/**
	 * Esegue la fattorizzazione Gaussiana 
	 * necessaria alla inversione della matrice passata al metodo inverti
	 * @param a Matrice a di double (matrice da invertire) 
	 * @param indice int[] vettore di lunghezza pari al numero di elementi della matrice 		 
	 */
 
		// Eseguo la fattorizzazione Gaussiana 
    public static void gaussian(double a[][], int indice[]) //indice conserva il pivot
    {
        int n = indice.length;
        double c[] = new double[n];
 
        for (int i=0; i<n; ++i) 
            indice[i] = i;
 
 		// Ricerca dei fattori di scaling per le singole righe
        for (int i=0; i<n; ++i) 
        {
            double c1 = 0;
            for (int j=0; j<n; ++j) 
            {
                double c0 = Math.abs(a[i][j]);
                if (c0 > c1) c1 = c0;
            }
            c[i] = c1;
        }
 
 		// Ricerca del pivot in ogni colonna
        int k = 0;
        for (int j=0; j<n-1; ++j) 
        {
            double pi1 = 0;
            for (int i=j; i<n; ++i) 
            {
                double pi0 = Math.abs(a[indice[i]][j]);
                pi0 /= c[indice[i]];
                if (pi0 > pi1) 
                {
                    pi1 = pi0;
                    k = i;
                }
            }
 
   			// Scambio di righe seguendo l'ordine di pivoting 
            int itmp = indice[j];
            indice[j] = indice[k];
            indice[k] = itmp;
            for (int i=j+1; i<n; ++i) 	
            {
                double pj = a[indice[i]][j]/a[indice[j]][j];
 
 			// Aggiorno i rapporti di pivoting al di sotto della diagonale principale
                a[indice[i]][j] = pj;
 
 			//Modifico gli altri elementi in base al rapporto di pivoting trovato in precenza 
                for (int l=j+1; l<n; ++l)
                    a[indice[i]][l] -= pj*a[indice[j]][l];
            }
        }
    }


/**
 * Restituisce la categoria della funzione Complessa 
 * @return Category  String di categoria della funzione
 */
// Metodo che restituisce la categoria di appartenenza della funzione.
    public final String getCategory() {
		return "Matrice";
	}
    
/**
 * Restituisce una breve descrizione della funzione complessa
 * @return Help, String di Help
 */
// Metodo che restituisce una breve spiegazione della funzione.
    public final String getHelp() {
		return "Crea la matrice inversa";
	} 
    
    /**
     * Restituisce il nome della funzione 
     * @return Name, String nome della funzione
     */
// Metodo che resituisce il nome della funzione su LibreOffice Calc.   
    public final String getName() {
		return "MATR.INVERSA";
	}
}

